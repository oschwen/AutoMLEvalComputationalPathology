#!/usr/bin/env bash

TIMESTAMP="$1"
GIT_HASH="$2"

cd /code
ls -laR /code
tar xvf dot_git.tar
rm -rf dot_git.tar
echo "checking out" $GIT_HASH
git checkout $GIT_HASH . || { echo 'checking out revision failed' ; exit 1; }

cd autogluon/actualCode
./_runInsideDocker.sh ${TIMESTAMP}_${GIT_HASH}
