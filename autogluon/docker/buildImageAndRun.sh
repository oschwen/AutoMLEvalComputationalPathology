#!/usr/bin/env bash

git pull
git push

./_buildDockerImage.sh

if [ "$#" -eq 1 ]
then
  GIT_HASH=$(git rev-parse --short $1)
else
  # use current local head
  GIT_HASH=$(git rev-parse --short HEAD)
fi

./_runLocallyInDocker.sh ${GIT_HASH}
