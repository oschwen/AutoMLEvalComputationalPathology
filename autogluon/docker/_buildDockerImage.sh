#!/usr/bin/env bash

cd "$(dirname "$0")"

cp -r ../../.git ./  # so that we can copy it into the image
tar cvf dot_git.tar .git

docker build -t autogluon-usage:latest .

rm -rf .git dot_git.tar
