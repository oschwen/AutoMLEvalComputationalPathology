#!/usr/bin/env bash

INPUT_DIR=/path/to/input_data  # ADAPT HERE
OUTPUT_DIR=/path/to/output_directory  # ADAPT HERE
TAGNAME=autogluon-usage:latest

TIMESTAMP=$(date +%y%m%d-%H%M%S)

# get revision short ID if provided (as ID, tag, branch, …), otherwise use HEAD
# note: this may need to be passed from the calling script, if any
if [ "$#" -eq 1 ]
then
  GIT_HASH=$(git rev-parse --short $1)
else
  # use current local head
  GIT_HASH=$(git rev-parse --short HEAD)
fi

docker run --rm --runtime=nvidia --shm-size=8G --user $(id -u):$(id -g) -v $INPUT_DIR:/inData -v $OUTPUT_DIR:/output $TAGNAME /prep/_setupAndRunInsideDocker.sh ${TIMESTAMP} ${GIT_HASH}
