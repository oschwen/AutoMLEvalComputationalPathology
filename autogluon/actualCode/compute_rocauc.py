#!/usr/bin/env python3

import os, sys
import csv, re
from collections import OrderedDict
from typing import Tuple, NamedTuple
import sklearn.metrics
import numpy
import tqdm


class ReferenceAndProbability(NamedTuple):
    referenceValue: int
    probability: float


def _referenceValueFromPath(path: str) -> int:
    reference_value = re.split(r'/', path)[-2]
    return int(reference_value)


def _probClassOne(class_name: str, score: str) -> float:
    if class_name == '1':
        return float(score)
    elif class_name == '0':
        return 1.0 - float(score)
    else:
        return numpy.nan


def extract_data_twoclass(csv_string: str) -> OrderedDict:
    csv_reader = csv.reader(csv_string.splitlines(), delimiter=',')
    next(csv_reader, None)  # skip the header
    data_per_file = OrderedDict()
    for row in csv_reader:
        _, class_name, score, _, path = row
        data_per_file[path] = ReferenceAndProbability(referenceValue = _referenceValueFromPath(path), probability=_probClassOne(class_name, score))  # possibly overwrites (same) previous result, OK for two-class case

    return data_per_file


def extract_data_threeclass(csv_string: str, current_class: int) -> OrderedDict:
    csv_reader = csv.reader(csv_string.splitlines(), delimiter=',')
    next(csv_reader, None)  # skip the header
    data_per_file = OrderedDict()
    for row in csv_reader:
        _, class_name, score, _, path = row
        if int(class_name) == current_class:
            reference_value = 1 if _referenceValueFromPath(path) == current_class else 0
            data_per_file[path] = ReferenceAndProbability(referenceValue=reference_value, probability=float(score))

    return data_per_file


def tile_to_slide_data(data_per_file: OrderedDict) -> OrderedDict:
    def _slideIDFromPath(path: str) -> str:
        filename = re.split(r'/', path)[-1]
        slideID_ = re.split(r'-TCGA-', filename)[-1]
        slideID_ = re.split(r'\.', slideID_)[0]
        slideID = re.split(r'_', slideID_)[0]
        return slideID

    references_per_slide = OrderedDict()
    pred_probs_per_slide = OrderedDict()
    for path, values in data_per_file.items():
        slideID = _slideIDFromPath(path)
        reference = values.referenceValue
        if slideID not in references_per_slide:
            references_per_slide[slideID] = []
        references_per_slide[slideID].append(reference)
        if slideID not in pred_probs_per_slide:
            pred_probs_per_slide[slideID] = []
        pred_prob = values.probability
        pred_probs_per_slide[slideID].append(pred_prob)

    data_per_slide = OrderedDict()
    for slideID, reference_values in references_per_slide.items():
        assert(len(set(reference_values)) == 1)
        data_per_slide[slideID] = ReferenceAndProbability(referenceValue=reference_values[0],
                                                          probability=numpy.mean(pred_probs_per_slide[slideID]))

    return data_per_slide


def _acc_ROC_AUC(reference_values: list, predictions: list) -> Tuple[float, float]:
    accuracy = sklearn.metrics.accuracy_score(reference_values, [1 if p > 0.5 else 0 for p in predictions])
    ROC_AUC = sklearn.metrics.roc_auc_score(reference_values, predictions)
    return accuracy, ROC_AUC

def _listsFromExtractedData(data_per_file: OrderedDict) -> Tuple[list, list]:
    reference_values = []
    predictions = []

    for entry in data_per_file.values():
        reference_values.append(entry.referenceValue)
        predictions.append(entry.probability)

    return reference_values, predictions


def _getMetrics(data_per_tile_or_slide: OrderedDict) -> Tuple[int, int, int, int, float, float]:
    def _getTPFPTNFN(reference_values: list, predictions: list) -> Tuple[int, int, int, int]:
        TP = 0
        FP = 0
        TN = 0
        FN = 0

        for ref, pred in zip(reference_values, predictions):
            if ref == 0 and pred < 0.5:
                TN = TN + 1
            elif ref == 0 and pred >= 0.5:
                FP = FP + 1
            elif ref == 1 and pred < 0.5:
                FN = FN + 1
            elif ref == 1 and pred >= 0.5:
                TP = TP + 1
            else:
                print(ref, pred)
                raise Exception("This should not happen.")

        return (TP, FP, TN, FN)

    reference_values, predictions = _listsFromExtractedData(data_per_tile_or_slide)
    TP, FP, TN, FN = _getTPFPTNFN(reference_values, predictions)
    accuracy, ROC_AUC = _acc_ROC_AUC(reference_values, predictions)
    return TP, FP, TN, FN, accuracy, ROC_AUC


def printMetrics(data_per_tile_or_slide: OrderedDict) -> None:
    TP, FP, TN, FN, accuracy, ROC_AUC = _getMetrics(data_per_tile_or_slide)
    TPR = TP/(TP+FN)
    FPR = FP/(FP+TN)
    print('  ROC_AUC: %f' % (ROC_AUC))
    print('TP: %d, FP: %d, TN: %d, FN: %d' % (TP, FP, TN, FN))
    print('TPR: %f, FPR: %f, Acc: %f, F1: %f' % (TPR, FPR, (TP+TN)/(TP+FP+FN+TN), 2*TP/(2*TP+FP+FN)))
    print('accuracy: %f' % (accuracy))


def printBootstrappingCIs(data_per_tile_or_slide: OrderedDict) -> None:
    def ci95(data):
        data_sorted = numpy.array(data)
        data_sorted.sort()
        return (data_sorted[int(0.025*len(data_sorted))], data_sorted[int(0.975*len(data_sorted))])

    n_bootstraps = 2000
    accuracies = []
    ROC_AUCs = []
    reference_values, predictions = _listsFromExtractedData(data_per_tile_or_slide)
    reference_values = numpy.array(reference_values)
    predictions = numpy.array(predictions)
    for _ in tqdm.tqdm(range(n_bootstraps), 'bootstrapping'):
        bootstrapping_indices = numpy.random.randint(0, len(reference_values), len(reference_values))
        reference_values_bootstrapped = reference_values[bootstrapping_indices]
        predictions_bootstrapped = predictions[bootstrapping_indices]
        accuracy, ROC_AUC = _acc_ROC_AUC(reference_values_bootstrapped, predictions_bootstrapped)
        accuracies.append(accuracy)
        ROC_AUCs.append(ROC_AUC)
    print('CI95 ROC_AUC', ci95(ROC_AUCs))
    print('CI95 accuracy', ci95(accuracies))


def printEvaluation(data_per_tile) -> None:
    print('Tile-based')
    printMetrics(data_per_tile)
    printBootstrappingCIs(data_per_tile)

    data_per_slide = tile_to_slide_data(data_per_tile)
    print('Slide-based')
    printMetrics(data_per_slide)
    printBootstrappingCIs(data_per_slide)


def _regressionTest() -> bool:
    exampleInput = """,class,score,id,image
0,1,0.7926809787750244,1,/inputData/Validation/subfolder0/0/blk-AAWMTDDVQNRA-TCGA-CA-6718-01A-01-BS1.png
1,1,0.5539991855621338,1,/inputData/Validation/subfolder0/0/blk-ACCTKGKRKCFT-TCGA-CA-6718-01A-01-BS1.png
2,0,0.8179009556770325,0,/inputData/Validation/subfolder0/0/blk-ACPLQKQSASIK-TCGA-CA-6718-01A-01-BS1.png
3,1,0.7063747644424438,1,/inputData/Validation/subfolder0/0/blk-ACSQCCAWGRVC-TCGA-CM-6680-01A-01-TS1.png
4,0,0.7511332631111145,0,/inputData/Validation/subfolder0/1/blk-ADPLPTHNCQSP-TCGA-CA-6717-01A-01-TS1.png
5,1,0.9048436284065247,1,/inputData/Validation/subfolder0/1/blk-ADRAVEDLPPIT-TCGA-CA-6717-01A-01-TS1.png
6,0,0.8912500143051147,0,/inputData/Validation/subfolder0/1/blk-AFRTDGVTCCYT-TCGA-CA-6717-01A-01-TS1.png
7,1,0.9884575009346008,1,/inputData/Validation/subfolder0/1/blk-AGCRWHSQVNYH-TCGA-AZ-6603-01A-01-TS1.png
8,1,0.9144551753997803,1,/inputData/Validation/subfolder0/1/blk-AGIDPGQQSIPE-TCGA-AZ-6603-01A-01-TS1.png
"""
    OK = True
    data_per_tile = extract_data_twoclass(exampleInput)
    TP, FP, TN, FN, accuracy, ROC_AUC = _getMetrics(data_per_tile)
    OK &= (TP == 3) and (FP == 3) and (TN == 1) and (FN == 2)
    OK &= abs(accuracy - 4/9) < 1.0e-6 and abs(ROC_AUC - 0.65) < 1.0e-6
    TP, FP, TN, FN, accuracy, ROC_AUC = _getMetrics(tile_to_slide_data(data_per_tile))
    OK &= (TP == 1) and (FP == 2) and (TN == 0) and (FN == 1)
    OK &= abs(accuracy - 0.25) < 1.0e-6 and abs(ROC_AUC - 0.5) < 1.0e-6
    return OK

if __name__ == '__main__':
    numpy.random.seed(20210307+1531)
    assert(_regressionTest())

    if sys.argv[2] == '--twoClass':
        with open(os.path.join(sys.argv[1]), 'r') as csv_file:
            data_per_tile = extract_data_twoclass(csv_file.read())
        printEvaluation(data_per_tile)

    elif sys.argv[2] == '--threeClass':
        for current_class in range(3):
            print(f'Current class {current_class}')
            with open(os.path.join(sys.argv[1]), 'r') as csv_file:
                data_per_tile = extract_data_threeclass(csv_file.read(), current_class)
            printEvaluation(data_per_tile)

    else:
        print("Illegal case")
