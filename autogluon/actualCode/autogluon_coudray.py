from autogluon_presets import _loadDatasetInChunks
SPLITTING_FACTOR = 40

def getOutputDir(ID: str) -> str:
    return '/output/Coudray_128_jpeg/autogluon-Coudray_%s' % (ID,)

_CoudrayTwoClasses = ['0', '1']
_CoudrayThreeClasses = ['0', '1', '2']

CoudrayClasses = {'--twoClass':   _CoudrayTwoClasses,
                  '--threeClass': _CoudrayThreeClasses}

def loadDatasetInChunks(purpose_name, classes):
    return _loadDatasetInChunks(purpose_name, SPLITTING_FACTOR, classes)
