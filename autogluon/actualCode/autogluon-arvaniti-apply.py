#!/usr/bin/env python3

from autogluon.vision import ImagePredictor
import os, sys

from autogluon_arvaniti import loadDatasetInChunks, getOutputDir

def evaluateDataset(classifier, dataset_folder, dataset_name, output_folder_name):
    dataset = loadDatasetInChunks(dataset_folder)
    print(dataset)

    prediction = classifier.predict(dataset)
    print(prediction)
    csv_filename = os.path.join(output_folder_name, '%s_predictions.csv' % (dataset_name,))
    prediction.to_csv(csv_filename)

    prediction_probabilities = classifier.predict_proba(dataset)
    print(prediction_probabilities)
    csv_filename = os.path.join(output_folder_name, '%s_prediction_probabilities.csv' % (dataset_name,))
    prediction_probabilities.to_csv(csv_filename)


if __name__ == "__main__":
    os.environ['MXNET_HOME'] = '/output/Arvaniti'
    os.environ['MXNET_CUDNN_AUTOTUNE_DEFAULT'] = '0'
    os.environ['MPLCONFIGDIR'] = '/output/Arvaniti'

    output_root_dir = getOutputDir(sys.argv[1])
    classifier = ImagePredictor.load(os.path.join(output_root_dir, 'image_predictor.ag'))

    evaluateDataset(classifier, 'Training',     'Training',     output_root_dir)
    evaluateDataset(classifier, 'Validation',   'Validation',   output_root_dir)
    evaluateDataset(classifier, 'Test/patho_1', 'Test_patho_1', output_root_dir)
    evaluateDataset(classifier, 'Test/patho_2', 'Test_patho_2', output_root_dir)
