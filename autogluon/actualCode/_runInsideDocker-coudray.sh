#!/usr/bin/env bash

SCRIPT_DIR="$(dirname "$(readlink -fm "$0")")"
SUFFIX="$1"
CONSOLE_OUTPUT="/output/Coudray_128_jpeg/console_${SUFFIX}.out"
OUTPUT_DIR="/output/Coudray_128_jpeg/autogluon-Coudray_${SUFFIX}"
mkdir ${OUTPUT_DIR}

echo "Running on node ${NODE_NAME}" | tee -a "${CONSOLE_OUTPUT}"
nvidia-smi                          | tee -a "${CONSOLE_OUTPUT}"

cd /inputData
unzip /inData/Coudray_128_jpeg/idc_input.zip   2>&1 > /dev/null  # | tee -a "${CONSOLE_OUTPUT}"
cd $SCRIPT_DIR

MXNET_HOME="/output"

# ADAPT HERE for other Coudray use case: --normal_tumor, --LUAD_LUSC, or --normal_LUAD_LUSC
#            for other Coudray use case: --twoClass or --threeClass
COUDRAY_CASE_ARGUMENT="--LUAD_LUSC"
NUM_CLASS_ARGUMENT="--twoClass"

$SCRIPT_DIR/autogluon-coudray-prepare.py $COUDRAY_CASE_ARGUMENT     2>&1 | tee -a "${CONSOLE_OUTPUT}"
mv /inputData/idc_input/* /inputData/                               2>&1 | tee -a "${CONSOLE_OUTPUT}"
rm -rf /inputData/idc_input /inputData/tiles                        2>&1 | tee -a "${CONSOLE_OUTPUT}"

$SCRIPT_DIR/autogluon-coudray-fit.py   $SUFFIX $NUM_CLASS_ARGUMENT  2>&1 | tee -a "${CONSOLE_OUTPUT}"
$SCRIPT_DIR/autogluon-coudray-apply.py $SUFFIX $NUM_CLASS_ARGUMENT  2>&1 | tee -a "${CONSOLE_OUTPUT}"

$SCRIPT_DIR/compute_rocauc.py "${OUTPUT_DIR}"/train_prediction_probabilities.csv $NUM_CLASS_ARGUMENT 2>&1 | tee -a "${CONSOLE_OUTPUT}"
$SCRIPT_DIR/compute_rocauc.py "${OUTPUT_DIR}"/valid_prediction_probabilities.csv $NUM_CLASS_ARGUMENT 2>&1 | tee -a "${CONSOLE_OUTPUT}"
$SCRIPT_DIR/compute_rocauc.py "${OUTPUT_DIR}"/test_prediction_probabilities.csv  $NUM_CLASS_ARGUMENT 2>&1 | tee -a "${CONSOLE_OUTPUT}"
