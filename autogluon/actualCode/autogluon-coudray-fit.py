#!/usr/bin/env python3

import os, sys

from autogluon.vision import ImagePredictor

from autogluon_coudray import getOutputDir, loadDatasetInChunks, CoudrayClasses
from autogluon_presets import *

if __name__ == "__main__":
    os.environ['MXNET_HOME'] = '/output/Coudray_128_jpeg'
    os.environ['MXNET_CUDNN_AUTOTUNE_DEFAULT'] = '0'
    os.environ['MPLCONFIGDIR'] = '/output/Coudray_128_jpeg'

    output_root_dir = getOutputDir(sys.argv[1])
    training_dataset = loadDatasetInChunks('train', CoudrayClasses[sys.argv[2]])
    validation_dataset = loadDatasetInChunks('valid', CoudrayClasses[sys.argv[2]])

    predictor = ImagePredictor(path=output_root_dir)

    # ADAPT HERE for using other preset
    classifier = predictor.fit(training_dataset, tuning_data=validation_dataset, nthreads_per_trial=8, presets=Preset_ShortRun)
    #classifier = predictor.fit(training_dataset, tuning_data=validation_dataset, nthreads_per_trial=8, presets=Preset_Default)
    #classifier = predictor.fit(training_dataset, tuning_data=validation_dataset, nthreads_per_trial=8, presets=Preset_3)
    #classifier = predictor.fit(training_dataset, tuning_data=validation_dataset, nthreads_per_trial=8, presets=Preset_18)
    #classifier = predictor.fit(training_dataset, tuning_data=validation_dataset, nthreads_per_trial=8, presets=Preset_34)
    #classifier = predictor.fit(training_dataset, tuning_data=validation_dataset, nthreads_per_trial=8, presets=Preset_50)
    #classifier = predictor.fit(training_dataset, tuning_data=validation_dataset, nthreads_per_trial=8, presets=Preset_101)
    #classifier = predictor.fit(training_dataset, tuning_data=validation_dataset, nthreads_per_trial=8, presets=Preset_HPO)

    fit_result = predictor.fit_summary()
    classifier.save(os.path.join(output_root_dir, 'image_predictor.ag'))
