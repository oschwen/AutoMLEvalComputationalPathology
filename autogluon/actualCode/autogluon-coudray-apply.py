#!/usr/bin/env python3

from autogluon.vision import ImagePredictor
import os, sys

from autogluon_coudray import loadDatasetInChunks, getOutputDir, CoudrayClasses

def evaluateDataset(classifier, dataset_name, output_folder_name, coudray_classes):
    dataset = loadDatasetInChunks(dataset_name, coudray_classes)
    print(dataset)

    prediction = classifier.predict(dataset)
    print(prediction)
    csv_filename = os.path.join(output_folder_name, '%s_predictions.csv' % (dataset_name,))
    prediction.to_csv(csv_filename)

    prediction_probabilities = classifier.predict_proba(dataset)
    print(prediction_probabilities)
    csv_filename = os.path.join(output_folder_name, '%s_prediction_probabilities.csv' % (dataset_name,))
    prediction_probabilities.to_csv(csv_filename)


if __name__ == "__main__":
    os.environ['MXNET_HOME'] = '/output/Coudray_128_jpeg'
    os.environ['MXNET_CUDNN_AUTOTUNE_DEFAULT'] = '0'
    os.environ['MPLCONFIGDIR'] = '/output/Coudray_128_jpeg'

    output_root_dir = getOutputDir(sys.argv[1])
    classifier = ImagePredictor.load(os.path.join(output_root_dir, 'image_predictor.ag'))

    evaluateDataset(classifier, 'test',  output_root_dir, CoudrayClasses[sys.argv[2]])
    evaluateDataset(classifier, 'valid', output_root_dir, CoudrayClasses[sys.argv[2]])
    evaluateDataset(classifier, 'train', output_root_dir, CoudrayClasses[sys.argv[2]])
