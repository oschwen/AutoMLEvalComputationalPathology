#!/usr/bin/env bash

SCRIPT_DIR="$(dirname "$(readlink -fm "$0")")"
SUFFIX="$1"
CONSOLE_OUTPUT="/output/Kather/console_${SUFFIX}.out"
OUTPUT_DIR="/output/Kather/autogluon-Kather_${SUFFIX}"
# ADAPT HERE for other sub-use case/dataset: CRC_DX, CRC_KR, STAD
DATASET_NAME="STAD"

echo "Running on node ${NODE_NAME}" | tee -a "${CONSOLE_OUTPUT}"
nvidia-smi                          | tee -a "${CONSOLE_OUTPUT}"

du -msc /inData/Kather/                                2>&1 | tee -a "${CONSOLE_OUTPUT}"

cd /inputData
mkdir Training
cd Training
unzip /inData/Kather/${DATASET_NAME}_TRAIN_MSIMUT.zip  > /dev/null
unzip /inData/Kather/${DATASET_NAME}_TRAIN_MSS.zip     > /dev/null
cd ..
mkdir Test
cd Test
unzip /inData/Kather/${DATASET_NAME}_TEST_MSIMUT.zip   > /dev/null
unzip /inData/Kather/${DATASET_NAME}_TEST_MSS.zip      > /dev/null
cd ..

du -msc /inputData/                                    2>&1 | tee -a "${CONSOLE_OUTPUT}"

mv /inputData/Training/MSS    /inputData/Training/0
mv /inputData/Training/MSIMUT /inputData/Training/1
mv /inputData/Test/MSS        /inputData/Test/0
mv /inputData/Test/MSIMUT     /inputData/Test/1

cd $SCRIPT_DIR

MXNET_HOME="/output"

$SCRIPT_DIR/autogluon-kather-prepare.py       2>&1 | tee -a "${CONSOLE_OUTPUT}"
$SCRIPT_DIR/autogluon-kather-fit.py $SUFFIX   2>&1 | tee -a "${CONSOLE_OUTPUT}"
$SCRIPT_DIR/autogluon-kather-apply.py $SUFFIX 2>&1 | tee -a "${CONSOLE_OUTPUT}"

$SCRIPT_DIR/compute_rocauc.py "${OUTPUT_DIR}"/Training_prediction_probabilities.csv   --twoClass  2>&1 | tee -a "${CONSOLE_OUTPUT}"
$SCRIPT_DIR/compute_rocauc.py "${OUTPUT_DIR}"/Validation_prediction_probabilities.csv --twoClass  2>&1 | tee -a "${CONSOLE_OUTPUT}"
$SCRIPT_DIR/compute_rocauc.py "${OUTPUT_DIR}"/Test_prediction_probabilities.csv       --twoClass  2>&1 | tee -a "${CONSOLE_OUTPUT}"
