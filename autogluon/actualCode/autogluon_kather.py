from autogluon_presets import _loadDatasetInChunks
SPLITTING_FACTOR = 40

def getOutputDir(ID: str) -> str:
    return '/output/Kather/autogluon-Kather_%s' % (ID,)

KatherClasses = ['0', '1']

def loadDatasetInChunks(purpose_name):
    return _loadDatasetInChunks(purpose_name, SPLITTING_FACTOR, KatherClasses)
