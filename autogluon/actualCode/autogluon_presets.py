from autogluon.core.space import Categorical, Real
from autogluon.vision import ImagePredictor
import pandas

SEC_PER_HOUR: int = 3600

Preset_ShortRun = [{
    'hyperparameters': {
        'model': 'resnet50_v1b',
        'lr': 0.01,
        'batch_size': 64,
        'epochs': 50,
        'early_stop_patience': 5,
    },

    'time_limit': 60,}]

Preset_Default = [{
    'hyperparameters': {
        'model': 'resnet50_v1b',
        'lr': 0.01,
        'batch_size': 64,
        'epochs': 50,
        'early_stop_patience': 5,
    },

    'time_limit': 1 * SEC_PER_HOUR,}]

Preset_3 = [{
    'hyperparameters': {
        'model': Categorical('mobilenetv3_small',),
        'lr': Categorical(0.01, 0.005, 0.001),
        'batch_size': Categorical(64, 128),
        'epochs': Categorical(50, 100),
        'early_stop_patience': 10,
    },
    'hyperparameter_tune_kwargs': {
        'num_trials': 12,
        'search_strategy': 'bayesopt'},
    'time_limit': 6 * SEC_PER_HOUR, }]

Preset_18 = [{
    'hyperparameters': {
        'model': Categorical('resnet18_v1b',),
        'lr':  Categorical(0.01, 0.005, 0.001),
        'batch_size': Categorical(64, 128),
        'epochs': Categorical(50, 100),
        'early_stop_patience': 10,
    },
    'hyperparameter_tune_kwargs': {
        'num_trials': 12,
        'search_strategy': 'bayesopt'},
    'time_limit': 6 * SEC_PER_HOUR, }]

Preset_34 = [{
    'hyperparameters': {
        'model': Categorical('resnet34_v1b'),
        'lr': Real(1e-4, 1e-2, log=True),
        'batch_size': Categorical(8, 16, 32, 64, 128),
        'epochs': 150,
        'early_stop_patience': 20,
    },
    'hyperparameter_tune_kwargs': {
        'num_trials': 16,
        'search_strategy': 'bayesopt'},
    'time_limit': 6 * SEC_PER_HOUR, }]

Preset_50 = [{
    'hyperparameters': {
        'model': 'resnet50_v1b',
        'lr': 0.01,
        'batch_size': 64,
        'epochs': 50,
        'early_stop_patience': 5,
    },

    'time_limit': 6 * SEC_PER_HOUR,}]

Preset_101 = [{
    'hyperparameters': {
        'model': Categorical('resnet101_v1d'),
        'lr': Real(1e-5, 1e-2, log=True),
        'batch_size': Categorical(32, 64, 128),
        'epochs': 200,
        'early_stop_patience': 50,
    },
    'hyperparameter_tune_kwargs': {
        'num_trials': 16,
        'search_strategy': 'bayesopt'},
    'time_limit': 6 * SEC_PER_HOUR,}]

Preset_HPO = [{
    'hyperparameters': {
        'model': Categorical('resnet18_v1b', ),
        'lr': Real(1e-4, 1e-2, log=True),
        'batch_size': Categorical(64, 128),
        'epochs': Categorical(50, 100),
        'early_stop_patience': 10,
    },
    'hyperparameter_tune_kwargs': {
        'num_trials': 16,
        'search_strategy': 'bayesopt'},
    'time_limit': 14 * 24 * SEC_PER_HOUR,}]


def _loadDatasetInChunks(purpose_name, splitting_factor, classes):
    datasets = [ImagePredictor.Dataset.from_folder('/inputData/%s/subfolder%d'%(purpose_name, i,)) for i in range(splitting_factor)]
    dataset = ImagePredictor.Dataset(pandas.concat(datasets, ignore_index=True), classes=classes)
    return dataset
