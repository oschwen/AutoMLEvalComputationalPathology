#!/usr/bin/env python3

from autogluon.vision import ImagePredictor
import os, sys

from autogluon_kather import loadDatasetInChunks, getOutputDir

def evaluateDataset(classifier, dataset_name, output_folder_name):
    dataset = loadDatasetInChunks(dataset_name)
    print(dataset)

    prediction = classifier.predict(dataset)
    print(prediction)
    csv_filename = os.path.join(output_folder_name, '%s_predictions.csv' % (dataset_name,))
    prediction.to_csv(csv_filename)

    prediction_probabilities = classifier.predict_proba(dataset)
    print(prediction_probabilities)
    csv_filename = os.path.join(output_folder_name, '%s_prediction_probabilities.csv' % (dataset_name,))
    prediction_probabilities.to_csv(csv_filename)


if __name__ == "__main__":
    os.environ['MXNET_HOME'] = '/output/Kather'
    os.environ['MXNET_CUDNN_AUTOTUNE_DEFAULT'] = '0'
    os.environ['MPLCONFIGDIR'] = '/output/Kather'

    output_root_dir = getOutputDir(sys.argv[1])
    classifier = ImagePredictor.load(os.path.join(output_root_dir, 'image_predictor.ag'))

    evaluateDataset(classifier, 'Test',       output_root_dir)
    evaluateDataset(classifier, 'Validation', output_root_dir)
    evaluateDataset(classifier, 'Training',   output_root_dir)
