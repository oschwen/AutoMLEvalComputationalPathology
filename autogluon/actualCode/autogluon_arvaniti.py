from autogluon_presets import _loadDatasetInChunks
SPLITTING_FACTOR = 1

def getOutputDir(ID: str) -> str:
    return '/output/Arvaniti/autogluon-Arvaniti_%s' % (ID,)

ArvanitiClasses = ['0', '3', '4', '5']

def loadDatasetInChunks(purpose_name: str):
    return _loadDatasetInChunks(purpose_name, SPLITTING_FACTOR, ArvanitiClasses)
