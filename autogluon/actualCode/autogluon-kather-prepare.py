#!/usr/bin/env python3

import os, random
from typing import List

from autogluon_kather import SPLITTING_FACTOR

def _getInitialDestinationDirectories(data_root_directory: str, purpose_name: str) -> List[str]:
    destination_directories = [os.path.join(data_root_directory, purpose_name)]

    for subfolder in range(SPLITTING_FACTOR):
        subfolder_name = 'subfolder%d' % (subfolder,)
        destination_directories.append(os.path.join(data_root_directory, purpose_name, subfolder_name))
    return destination_directories


def _getTestFilesToMove(data_root_directory: str, purpose_name: str):
    file_source_destination_mapping = []
    additional_destination_directories = set()

    purpose_dir = os.path.join(data_root_directory, purpose_name)
    for class_dir, _, _ in os.walk(purpose_dir):
        class_name = class_dir.split('/')[-1]
        subfolder = 0
        for filename in [f for f in os.listdir(class_dir) if os.path.isfile(os.path.join(class_dir, f))]:
            sub_dir = os.path.join(purpose_dir, 'subfolder%d'%(subfolder), class_name)
            additional_destination_directories.add(sub_dir)
            source_path = os.path.join(class_dir, filename)
            dest_path = os.path.join(sub_dir, filename)
            file_source_destination_mapping.append((source_path, dest_path))
            subfolder = (subfolder + 1) % SPLITTING_FACTOR

    destination_directories = _getInitialDestinationDirectories(data_root_directory, purpose_name)
    destination_directories += [d for d in additional_destination_directories]
    return destination_directories, file_source_destination_mapping


def _split_dataset(file_source_destination_mapping):
    # use 85% for training, 12.5 for validation, 2.5 not at all
    random.shuffle(file_source_destination_mapping)
    p85  = int(round(0.85 * len(file_source_destination_mapping)))
    file_source_destination_mapping_train = file_source_destination_mapping[:p85]
    file_source_destination_mapping_valid = file_source_destination_mapping[p85:int(round(0.975 * len(file_source_destination_mapping)))]
    file_source_destination_mapping_valid = [(i[0], i[1].replace('/Training/', '/Validation/')) for i in file_source_destination_mapping_valid]
    return file_source_destination_mapping_train, file_source_destination_mapping_valid


def _moveFiles(destination_directories, file_source_destination_mapping):
    for destination_directory in destination_directories:
        if not os.path.exists(destination_directory):
            os.mkdir(destination_directory)

    for src, dst in file_source_destination_mapping:
        if os.path.isfile(src):
            os.rename(src, dst)
        else:
            print(src, 'does not exist and cannot be moved/made available')


def moveAndSplitTrainingData():
    destination_directories_train, file_source_destination_mapping = _getTestFilesToMove('/inputData', 'Training')
    destination_directories_valid = ['/inputData/Validation']
    destination_directories_valid += [d.replace('/Training/', '/Validation/') for d in destination_directories_train]
    file_source_destination_mapping_train, file_source_destination_mapping_valid = _split_dataset(file_source_destination_mapping)
    _moveFiles(destination_directories_train, file_source_destination_mapping_train)
    _moveFiles(destination_directories_valid, file_source_destination_mapping_valid)


def moveAndSplitTestData():
    destination_directories, file_source_destination_mapping = _getTestFilesToMove('/inputData', 'Test')
    _moveFiles(destination_directories, file_source_destination_mapping)


if __name__ == '__main__':
    os.environ['MPLCONFIGDIR'] = '/output/Kather'
    random.seed(20210630+1616)
    moveAndSplitTrainingData()
    moveAndSplitTestData()
