#!/usr/bin/env python3

import os, csv, sys

from autogluon_coudray import SPLITTING_FACTOR

def getFilesToMove(csv_filename, purpose_name, data_root_directory):
    file_source_destination_mapping = dict()
    destination_directories = [os.path.join(data_root_directory, purpose_name)]

    for subfolder in range(SPLITTING_FACTOR):
        subfolder_name = 'subfolder%d' % (subfolder,)
        destination_directories.append(os.path.join(data_root_directory, purpose_name, subfolder_name))

    subfolder = 0
    with open(os.path.join(data_root_directory, csv_filename)) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        next(csv_reader, None)  # skip the header
        for row in csv_reader:
            subfolder_name = 'subfolder%d' % (subfolder,)
            filename, classname = row
            destination_directory = os.path.join(data_root_directory, purpose_name, subfolder_name, classname)
            destination_directories.append(destination_directory)
            filename_modified = '/'.join(filename.split('/')[1:])  # drop leading "tiles/"
            filename_modified = filename_modified.replace('/', '_').replace('.', '_').replace('_jpeg', '.jpeg')  # flatten folder hierarchy and keep only one '.'
            file_source_destination_mapping[os.path.join(data_root_directory,filename)] = os.path.join(destination_directory, filename_modified)
            subfolder = (subfolder + 1) % SPLITTING_FACTOR

    return destination_directories, file_source_destination_mapping

def moveFiles(destination_directories, file_source_destination_mapping):
    for destination_directory in destination_directories:
        if not os.path.exists(destination_directory):
            os.mkdir(destination_directory)

    for src, dst in file_source_destination_mapping.items():
        if os.path.isfile(src):
            os.rename(src, dst)
        else:
            print(src, 'does not exist and cannot be moved/made available')


_csv_filename_mask = {'--normal_tumor': 'csv_%s_norm_cancer.csv',
                     '--LUAD_LUSC': 'csv_%s_luad_lusc.csv',
                     '--normal_LUAD_LUSC': 'csv_%s_norm_luad_lusc.csv'}


if __name__ == '__main__':
    os.environ['MPLCONFIGDIR'] = '/output/Coudray_128_jpeg'

    csv_filename_mask = _csv_filename_mask[sys.argv[1]]

    for purpose_name in ('test', 'train', 'valid'):
        destination_directories, file_source_destination_mapping = getFilesToMove(csv_filename_mask % (purpose_name,), purpose_name, '/inputData/idc_input/')
        moveFiles(destination_directories, file_source_destination_mapping)
