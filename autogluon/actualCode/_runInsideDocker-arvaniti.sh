#!/usr/bin/env bash

SCRIPT_DIR="$(dirname "$(readlink -fm "$0")")"
SUFFIX="$1"
CONSOLE_OUTPUT="/output/Arvaniti/console_${SUFFIX}.out"
OUTPUT_DIR="/output/Arvaniti/autogluon-Arvaniti_${SUFFIX}"
mkdir ${OUTPUT_DIR}

echo "Running on node ${NODE_NAME}" | tee -a "${CONSOLE_OUTPUT}"
nvidia-smi                          | tee -a "${CONSOLE_OUTPUT}"

cd /inputData
mkdir Validation Validation/0 Validation/3 Validation/4 Validation/5

mkdir Training
cd Training
unzip /inData/Arvaniti/train_validation_patches_250.zip  > /dev/null

# move validation dataset (according to paper) to Validation folder
mv 0/ZT76* ../Validation/0/
mv 3/ZT76* ../Validation/3/
mv 4/ZT76* ../Validation/4/
mv 5/ZT76* ../Validation/5/
cd ..

mkdir Test
cd Test
unzip /inData/Arvaniti/test_patches_250.zip              > /dev/null
cd ..

cd $SCRIPT_DIR

$SCRIPT_DIR/autogluon-arvaniti-prepare.py       2>&1 | tee -a "${CONSOLE_OUTPUT}"
# zip -r0v ${OUTPUT_DIR}/generatedTiles.zip /inputData  # output augmented images for checking/visualization purposes
$SCRIPT_DIR/autogluon-arvaniti-fit.py $SUFFIX   2>&1 | tee -a "${CONSOLE_OUTPUT}"
$SCRIPT_DIR/autogluon-arvaniti-apply.py $SUFFIX 2>&1 | tee -a "${CONSOLE_OUTPUT}"

echo "Training" | tee -a "${CONSOLE_OUTPUT}"
$SCRIPT_DIR/compute_cohens_kappa.py "${OUTPUT_DIR}"/Training_predictions.csv                                                  2>&1 | tee -a "${CONSOLE_OUTPUT}"

echo "Validation" | tee -a "${CONSOLE_OUTPUT}"
$SCRIPT_DIR/compute_cohens_kappa.py "${OUTPUT_DIR}"/Validation_predictions.csv                                                2>&1 | tee -a "${CONSOLE_OUTPUT}"

echo "Test (prediction vs 1, prediction vs 2, 1 vs 2)" | tee -a "${CONSOLE_OUTPUT}"
$SCRIPT_DIR/compute_cohens_kappa.py "${OUTPUT_DIR}"/Test_patho_1_predictions.csv "${OUTPUT_DIR}"/Test_patho_2_predictions.csv 2>&1 | tee -a "${CONSOLE_OUTPUT}"
