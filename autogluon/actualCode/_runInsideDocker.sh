#!/usr/bin/env bash

# ADAPT HERE: select use case to be run
./_runInsideDocker-coudray.sh "$@"
./_runInsideDocker-kather.sh "$@"
./_runInsideDocker-arvaniti.sh "$@"
