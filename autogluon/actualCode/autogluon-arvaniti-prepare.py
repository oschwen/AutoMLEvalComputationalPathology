#!/usr/bin/env python3

import os, random, numpy, tqdm, shutil
from typing import Dict, List, NamedTuple
import PIL.Image
import albumentations
import hashlib

from autogluon_arvaniti import SPLITTING_FACTOR, ArvanitiClasses

class StartPos(NamedTuple):
    x: int
    y: int

class AugmentationTask(NamedTuple):
    filename: str
    target_directory: int
    start_position: StartPos
    flip: bool
    flop: bool
    rot90: bool
    color_augment: bool

OVERSAMPLING_FACTOR = 8.0
HUE_MAX_SHIFT = 20
SAT_MAX_SHIFT = 30
VAL_MAX_SHIFT = 20

def getPresentFilesByClass(input_path: str) -> Dict[str, List[str]]:
    training_data = {i: [] for i in ArvanitiClasses}
    for class_dir, _, filenames in os.walk(input_path):
        class_name = class_dir.split('/')[-1]
        if class_name in ArvanitiClasses:
            training_data[class_name] += filenames
    return training_data


def _balance(filenames: List[str], max_num_files_per_class: int) -> List[str]:
    current_length = len(filenames)
    balanced_filenames = max_num_files_per_class//current_length * filenames
    balanced_filenames += random.sample(filenames, max_num_files_per_class - current_length * (max_num_files_per_class // current_length))
    return balanced_filenames


def _getAugmentationTasks(files_per_class: Dict[str, List[str]], directory: str) -> List[AugmentationTask]:
    max_num_files_per_class = int(OVERSAMPLING_FACTOR * numpy.max([len(f) for f in files_per_class.values()]))
    augmentationTasks: List[AugmentationTask] = []

    subfolder = 0
    for class_name, filenames_ in files_per_class.items():
        filenames = _balance(filenames_, max_num_files_per_class)
        for filename in filenames:
            augmentationTasks.append(
                AugmentationTask(filename=os.path.join(directory, class_name, filename),
                                 target_directory=subfolder,
                                 start_position=StartPos(random.randrange(250 - 224), random.randrange(250 - 224)),
                                 flip=bool(random.getrandbits(1)),
                                 flop=bool(random.getrandbits(1)),
                                 rot90=bool(random.getrandbits(1)),
                                 color_augment=True,
                                 ))
            subfolder = (subfolder + 1) % SPLITTING_FACTOR

    return augmentationTasks


def _getNonAugmentationTasks(files_per_class: Dict[str, List[str]], directory: str) -> List[AugmentationTask]:
    non_augmentation_tasks: List[AugmentationTask] = []

    subfolder = 0
    for class_name, filenames in files_per_class.items():
        for filename in filenames:
            non_augmentation_tasks.append(
                AugmentationTask(filename=os.path.join(directory, class_name, filename),
                                 target_directory=subfolder,
                                 start_position=StartPos((250 - 224)//2, (250 - 224)//2),
                                 flip=False,
                                 flop=False,
                                 rot90=False,
                                 color_augment=False,
                                 ))
            subfolder = (subfolder + 1) % SPLITTING_FACTOR

    return non_augmentation_tasks


def _runPreparationTasks(augmentation_tasks: List[AugmentationTask]) -> None:
    def _loadImage(filename: str) -> numpy.array:
        image = PIL.Image.open(filename)
        return numpy.array(image)

    def _cropImage(image: numpy.array, start_pos: StartPos) -> numpy.array:
        return image[start_pos.x:start_pos.x + 224, start_pos.y:start_pos.y + 224]

    def _saveImage(image: numpy.array, filename: str) -> None:
        _createOutputSubfolders(filename)
        PIL.Image.fromarray(image).save(filename)

    def _createOutputSubfolders(target_filename) -> None:
        target_folder = '/'.join(target_filename.split('/')[:-1])
        os.makedirs(target_folder, exist_ok=True)

    def getAugmentedPatch(augmentation_task: AugmentationTask) -> numpy.array:
        image = _loadImage(augmentation_task.filename)
        image = _cropImage(image, augmentation_task.start_position)

        if (augmentation_task.flip):
            image = numpy.fliplr(image)
        if (augmentation_task.flop):
            image = numpy.flipud(image)
        if (augmentation_task.rot90):
            image = numpy.rot90(image)

        if augmentation_task.color_augment:
            aug = albumentations.augmentations.transforms.HueSaturationValue(hue_shift_limit=HUE_MAX_SHIFT, sat_shift_limit=SAT_MAX_SHIFT, val_shift_limit=VAL_MAX_SHIFT, always_apply=True)
            image = aug(image=image)['image']

        return image

    for augmentation_task in tqdm.tqdm(augmentation_tasks, 'preparing tiles'):
        image = getAugmentedPatch(augmentation_task)
        output_folder = os.path.join('/'.join(augmentation_task.filename.split('/')[:-2]),
                                     'subfolder%d'%(augmentation_task.target_directory,),
                                     '/'.join(augmentation_task.filename.split('/')[-2]))
        suffix = hashlib.md5(numpy.ascontiguousarray(image)).hexdigest()
        target_filename = '.'.join(augmentation_task.filename.split('/')[-1].split('.')[:-1]) + '_' + suffix + '.' + augmentation_task.filename.split('/')[-1].split('.')[-1]
        _saveImage(image, os.path.join(output_folder, target_filename))


def _prepareTrainingOrValidationData(directory: str, augmentation: bool) -> None:
    files_per_class = getPresentFilesByClass(directory)
    augmentation_tasks = _getAugmentationTasks(files_per_class, directory) if augmentation else _getNonAugmentationTasks(files_per_class, directory)
    _runPreparationTasks(augmentation_tasks)
    # delete "original" 250×250 images which are no longer needed
    for subdir in ArvanitiClasses:
        shutil.rmtree(os.path.join(directory, subdir))


def prepareTrainingData(directory: str) -> None:
    _prepareTrainingOrValidationData(directory, augmentation=True)


def prepareValidationData(directory: str) -> None:
    _prepareTrainingOrValidationData(directory, augmentation=False)


def prepareTestData(directory_: str) -> None:
    for patho_dir in ('patho_1', 'patho_2'):
        directory = os.path.join(directory_, patho_dir)
        files_per_class = getPresentFilesByClass(directory)
        non_augmentation_tasks = _getNonAugmentationTasks(files_per_class, directory)
        _runPreparationTasks(non_augmentation_tasks)
        # delete "original" 250×250 images which are no longer needed
        for subdir in ArvanitiClasses:
            shutil.rmtree(os.path.join(directory, subdir))


if __name__ == '__main__':
    os.environ['MPLCONFIGDIR'] = '/output/Arvaniti'
    random.seed(20210709+1356)
    prepareTrainingData('/inputData/Training')
    prepareValidationData('/inputData/Validation')
    prepareTestData('/inputData/Test/')
