#!/usr/bin/env python3

import os, sys
import csv, re
from collections import OrderedDict
from typing import Tuple, NamedTuple
import sklearn.metrics
import numpy
import tqdm


class ReferenceAndProbability(NamedTuple):
    referenceValue: int
    predictedValue: int


def extractData(csv_string: str) -> OrderedDict:
    def _referenceValueFromPath(path: str) -> int:
        reference_value = re.split(r'/', path)[-2]
        return int(reference_value)

    csv_reader = csv.reader(csv_string.splitlines(), delimiter=',')
    next(csv_reader, None)  # skip the header
    data_per_file = OrderedDict()
    for row in csv_reader:
        _, prediction, _, _, path = row
        filename = path.split('/')[-1]
        data_per_file[filename] = ReferenceAndProbability(referenceValue = _referenceValueFromPath(path), predictedValue=int(prediction))

    return data_per_file


def _arrayFromExtractedData(data_per_file: OrderedDict) -> Tuple[list, list]:
    reference_values = []
    predictions = []

    for entry in data_per_file.values():
        reference_values.append(entry.referenceValue)
        predictions.append(entry.predictedValue)

    return numpy.array(reference_values, dtype=numpy.float), numpy.array(predictions, dtype=numpy.float)


def printConfusionMatrixAndKappa(reference_values: numpy.array, predictions: numpy.array) -> None:
    confusion_matrix = sklearn.metrics.confusion_matrix(reference_values, predictions)
    print('Confusion matrix\n', confusion_matrix)
    recalls = numpy.diag(confusion_matrix) / numpy.sum(confusion_matrix, axis = 1)
    macro_average_recall = numpy.mean(recalls)
    print('Macro-average recall:', macro_average_recall)
    cohen_kappa = sklearn.metrics.cohen_kappa_score(reference_values, predictions, weights='quadratic')
    print('Cohens kappa:', cohen_kappa)


def printCI95Kappa(reference_values: numpy.array, predictions: numpy.array) -> None:
    def ci95(data):
        data_sorted = numpy.array(data)
        data_sorted.sort()
        return (data_sorted[int(0.025*len(data_sorted))], data_sorted[int(0.975*len(data_sorted))])

    n_bootstraps = 2000
    kappas = []
    for _ in tqdm.tqdm(range(n_bootstraps), 'bootstrapping'):
        bootstrapping_indices = numpy.random.randint(0, len(reference_values), len(reference_values))
        reference_values_bootstrapped = reference_values[bootstrapping_indices]
        predictions_bootstrapped = predictions[bootstrapping_indices]
        cohen_kappa = sklearn.metrics.cohen_kappa_score(reference_values_bootstrapped, predictions_bootstrapped, weights='quadratic')
        kappas.append(cohen_kappa)
    print('CI95 Cohens kappa:', ci95(kappas))


def printMetrics(data_per_tile: OrderedDict) -> None:
    reference_values, predictions = _arrayFromExtractedData(data_per_tile)
    printConfusionMatrixAndKappa(reference_values, predictions)
    printCI95Kappa(reference_values, predictions)


def printPathologistMetrics(data_per_tile_1: OrderedDict, data_per_tile_2: OrderedDict) -> None:
    joint_files = set(data_per_tile_1.keys()).intersection(set(data_per_tile_2.keys()))
    reference_values_1, _ = _arrayFromExtractedData(OrderedDict((i, data_per_tile_1[i]) for i in joint_files))
    reference_values_2, _ = _arrayFromExtractedData(OrderedDict((i, data_per_tile_2[i]) for i in joint_files))
    printConfusionMatrixAndKappa(reference_values_1, reference_values_2)
    printCI95Kappa(reference_values_1, reference_values_2)

if __name__ == '__main__':
    numpy.random.seed(20210307+1531)

    with open(os.path.join(sys.argv[1]), 'r') as csv_file:
        data_per_tile_1 = extractData(csv_file.read())
    printMetrics(data_per_tile_1)

    if len(sys.argv) > 2:
        with open(os.path.join(sys.argv[2]), 'r') as csv_file:
            data_per_tile_2 = extractData(csv_file.read())
        printMetrics(data_per_tile_2)
        printPathologistMetrics(data_per_tile_1, data_per_tile_2)
