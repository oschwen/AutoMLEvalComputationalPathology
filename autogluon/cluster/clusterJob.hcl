job "autogluon-PLACEHOLDER_TIMESTAMP_PLACEHOLDER_GIT_HASH" {

  datacenters = ["cluster"]
  type = "batch"

  group "training-task-group" {

    ephemeral_disk {
      size    = 32768  # MiB, i.e., 32GiB
    }

    task "training-main" {
      leader = true
      driver = "docker"
      config {
        image = "url.for.registry/autogluon-usage:latest"  # ADAPT HERE

        command = "/prep/_setupAndRunInsideDocker.sh"
        args = ["PLACEHOLDER_TIMESTAMP", "PLACEHOLDER_GIT_HASH"]

        volumes = [
          "/path/to/input/data:/inData:ro",  # ADAPT HERE
          "/path/to/output/data:/output"  # ADAPT HERE
        ]

        shm_size = 8589934592  # Bytes, i.e., 8GiB
      }

      resources {
        cpu = 1000  # MHz
        memory = 32768  # MiB, i.e., 32GiB

        device "nvidia/gpu" {
          count = 1
        }
      }

      env {
        NODE_NAME = "${node.unique.name}"
      }
    }

    restart {
      attempts = 0
      mode = "fail"
    }
  }
}
