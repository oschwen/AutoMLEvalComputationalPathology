#!/usr/bin/env bash

cd ../docker
./_buildDockerImage.sh
cd ../cluster

docker rmi url.for.registry/autogluon-usage:latest  # ADAPT HERE
docker tag autogluon-usage:latest url.for.registry/autogluon-usage:latest  # ADAPT HERE
docker push url.for.registry/autogluon-usage:latest  # ADAPT HERE

if [ "$#" -eq 1 ]
then
  GIT_HASH=$(git rev-parse --short $1)
else
  # use current local head
  GIT_HASH=$(git rev-parse --short HEAD)
fi

./_runInCluster.sh ${GIT_HASH}
