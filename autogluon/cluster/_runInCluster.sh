#!/usr/bin/env bash

# get revision short ID if provided (as ID, tag, branch, …), otherwise use HEAD
if [ "$#" -eq 1 ]
then
  GIT_HASH=$(git rev-parse --short $1)
else
  # use current local head
  GIT_HASH=$(git rev-parse --short HEAD)
fi
TIMESTAMP=$(date +%y%m%d-%H%M%S)

# prepare nomad job
TEMP_HCL_FILE=$(mktemp --suffix=.hcl)
cat clusterJob.hcl | sed "s/PLACEHOLDER_TIMESTAMP/${TIMESTAMP}/g" | sed "s/PLACEHOLDER_GIT_HASH/${GIT_HASH}/g" > ${TEMP_HCL_FILE}

# run nomad job
nomad run -address=http://url.to.cluster:port -detach ${TEMP_HCL_FILE}  # ADAPT HERE

# clean up
rm ${TEMP_HCL_FILE}
