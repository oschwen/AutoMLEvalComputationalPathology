This code belongs to the publication L. O. Schwen, D. Schacherer, C. Geißler, and A. Homeyer: Evaluating generic AutoML tools for computational pathology, _Informatics in Medicine Unlocked_ 29 (100853), 2022, [DOI 10.1016/j.imu.2022.100853](https://doi.org/10.1016/j.imu.2022.100853).

Using the code requires Docker (tested with version 20.10.5).

The code is subject to the MIT license, see `LICENSE`.

AutoGluon
=========

Folder overview
---------------

~~~
autogluon
├── actualCode: folder containing the preprocessing, training, and evaluation for the tree use cases (arvaniti: Glesaon grading, coudray: tissue classification, kather: mutation prediction)
│   ├── autogluon-{arvaniti,coudray,kather}{,-apply,-fit,-prepare}.py: (no suffix): use-case-specific definitions, -apply: inference using trained network, -fit: training, -prepare: preprocessing datasets
│   ├── autogluon_presets.py: presets for AutoGluon
│   ├── compute_cohens_kappa.py: metric for the Gleason grading use case
│   ├── compute_rocauc.py: metric for the mutation prediction and tissue classification use cases
│   ├── _runInsideDocker-{arvaniti,coudray,kather}.sh: runs all steps for the respective use case: preprocessing (in addition to what the corresponding -prepare.py does), fitting, inference, evaluating metrics
│   └── _runInsideDocker.sh: calls one or more use cases (*)
├── cluster: folder for running the code on a nomad cluster
│   ├── buildImageAndRun.sh: builds the Docker image and starts a cluster job (*)
│   ├── clusterJob.hcl: describes the cluster job
│   └── _runInCluster.sh: starts the cluster job
└── docker: folder for defining a docker image and entrypoint in which the code can be run
    ├── _buildDockerImage.sh: builds the docker image
    ├── buildImageAndRun.sh: builds the docker image and starts a container runing the code (*)
    ├── Dockerfile: defines the Docker image for running the AutoGluon code
    ├── _runLocallyInDocker.sh: starts the container running the code locally, i.e., not on a cluster
    └── _setupAndRunInsideDocker.sh: script run inside the docker container to make the code available for running
(*) marks the "top level" script for each directory
~~~

Usage
-----

1. Prepare data in expected format
2. Edit all relevant files marked by `ADAPT HERE`, putting files in suitable locations and selecting which use case(s)/tasks to run
3. Run
   1. For local use with Docker: `cd docker && ./buildImageAndRun.sh`
   2. For cluster use: `cd cluster && ./buildImageAndRun.sh`


Google AutoML Vision
====================

Folder overview
---------------

~~~
googleAutoMLVision
├── buildImageAndRun.sh: builds a docker image and calls evaluate_tflite_model.py
├── Dockerfile: Defines the Docker image for running evaluate_tflite_model.py
└── evaluate_tflite_model.py : loads an exported .tflite model and runs inference on test data, the last folder in the hierarchy encodes the reference result
~~~

Usage
-----

1. Prepare data and download model trained by AutoML Vision in `tflite` format
2. Edit files at the locations marked by `ADAPT HERE`, putting files above in suitable locations
3. Call `buildImageAndRun.sh`
4. Use `autogluon/actualCode/compute_rocauc.py` or `autogluon/actualCode/compute_cohens_kappa.py` (as above) to compute metrics


Disclaimer
==========

Not for clinical use, may be used for research purposes only.
Fraunhofer MEVIS does not ensure compliance to medical product regulations for the software. Delivered ‘as is’ without specific verification or validation.
