#!/usr/bin/env python3

import csv, os

import tensorflow as tf
from PIL import Image
import numpy as np
from collections import OrderedDict
from tqdm import tqdm
from typing import List, Tuple

def load_tflite_model(model_filename: str) -> tf.lite.Interpreter:
    interpreter = tf.lite.Interpreter(model_filename)
    interpreter.allocate_tensors()
    return interpreter


def load_image(image_filename: str, width: int, height: int) -> np.array:
    image = Image.open(image_filename)
    image = image.resize((width, height)).convert('RGB')  # might be unnecessary if images are already RGB and 224×224
    image = np.array(image)
    array = image.reshape((1, width, height, 3))
    return array


def run_inference(interpreter: tf.lite.Interpreter, input_details, output_details, array: np.array) -> np.array:
    input_index = input_details[0]["index"]
    interpreter.set_tensor(input_index, array)
    interpreter.invoke()
    result = interpreter.get_tensor(output_details[0]['index'])
    result = np.squeeze(result)
    result = output_details[0]['quantization'][0] * result  # model is quantized, convert range of predictions to 0, 1
    return result


def check_ambiguity(result) -> None:
    result_sorted = sorted(result, reverse=True)
    if result_sorted[1] == result_sorted[0]:
        print('more than one maximum in', result)


def get_image_files_classes(csv_filename: str) -> List[List[str]]:
    filenames_classes: List[List[str, str]] = []
    csv_reader = None
    with open(csv_filename, 'r') as csv_file:
        csv_reader = csv.reader(csv_file.read().splitlines(), delimiter=',')
    for row in csv_reader:
        path, classname = row
        if path != 'path':  # skip header
            filenames_classes.append([path.replace('tiles/', '/inputData/tiles/'), classname])  # for compatibility with AutoGluon evaluation output
    return filenames_classes


def encode_classname_in_path(filename:str, classname:str) -> str:
    return filename.replace('/inputData/tiles/', '').replace('/', '_').replace('20.0', '20_0').replace('TCGA', f'/inputData/dummy/folder0/{classname}/TCGA')  # for compatibility with AutoGluon evaluation output


def save_predictions_and_probabilities(model_name: str, pathologist_number: str, results: OrderedDict,
                                       classes: Tuple[int, ...], class_order: Tuple[int, ...], classes_by_index: Tuple[int, ...]) -> None:
    # save in same format as AutoGluon does

    with open(f'/output/{model_name}_{pathologist_number}_prediction_probabilities.csv', 'w') as csvfile:
        csvfile.write(',class,score,id,image\n')
        firstcol = 0
        for image_filename, result in results.items():
            prob = [result[class_order[i]] for i in range(len(classes))]
            for classindex, classname in enumerate(classes):
                csvfile.write(f'{firstcol},{classname},{prob[classindex]},{classindex},{image_filename}\n')
                firstcol += 1

    with open(f'/output/{model_name}_{pathologist_number}_predictions.csv', 'w') as csvfile:
        csvfile.write(',class,score,id,image\n')
        firstcol = 0
        for image_filename, result in results.items():
            check_ambiguity(result)
            max_class_index = np.argmax(result)
            max_class_name = classes_by_index[max_class_index]
            prob = result[max_class_index]
            csvfile.write(f'{firstcol},{max_class_name},{prob},{max_class_index},{image_filename}\n')
            firstcol += 1


if __name__ == '__main__':
    model_name = 'ModelName'  # ADAPT HERE, this is one model exported from Google AutoML Vision (e.g., timestamp of model training and timestamp of export of the form 20210902010939-2021-09-03T08:32:57.724412Z
    interpreter = load_tflite_model(f'/path/to/exported/models/prefix_{model_name}/model.tflite')  # ADAPT HERE
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()
    print(input_details, '\n', output_details)

    height = input_details[0]['shape'][1]
    width = input_details[0]['shape'][2]

    classes = (0, 3, 4, 5)  # ADAPT HERE
    class_order = (1, 3, 2, 0)  # indices to obtain the order as follows  # ADAPT HERE
    classes_by_index = (3, 5, 4, 0)  # order as given in dict.txt next to model.tflite  # ADAPT HERE

    # ADAPT HERE: drop this loop if only one set of reference data is available
    for patho_num in [f'patho_{i + 1}' for i in range(2)]:  # pathologist count is 1-based
        results = OrderedDict()

        for directory in [f'/inputData/{group}/{digit}' for group in [f'Test/{patho_num}/subfolder0'] for digit in [f'{c}' for c in classes]]:
            for image_fn in tqdm(os.listdir(directory)):
                image_filename = os.path.join(directory, image_fn)
                array = load_image(image_filename, width, height)
                result = run_inference(interpreter, input_details, output_details, array)
                results[image_filename] = result

        save_predictions_and_probabilities(model_name, patho_num, results, classes, class_order, classes_by_index)
