#!/usr/bin/env bash

MODEL_DIR=/path/to/exported_models  # ADAPT HERE
DATA_DIR=/path/to/data  # ADAPT HERE
OUTPUT_DIR=$(pwd)  # ADAPT HERE

TAGNAME=autoaivision-evaluation:latest

docker build -t $TAGNAME .

docker run --runtime=nvidia --rm --user $(id -u):$(id -g) -v $MODEL_DIR:/models:ro -v $DATA_DIR:/inputData:ro -v $OUTPUT_DIR:/output $TAGNAME /code/evaluate_tflite_model.py ${ANALYSIS_TO_RUN}
